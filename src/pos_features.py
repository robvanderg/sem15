
def get_tag(sent, idx, find, boolean):
	ne = []
	words = []
	for word in sent:	
		if (word.split('/')[1].startswith(find)) != boolean:
			ne.append(word.split('/')[idx])
			words.append(word[:word.find('/')])
	return ne, words

def ne_overlap(sent1, sent2):
	ne1, word1 = get_tag(sent1, 1, 'O', True)
	ne2, word2 = get_tag(sent2, 1, 'O', True)

	mutual = 0
	only1 = 0
	only2 = 0
	for ne in ne1:
		if ne in ne2:
			mutual += 1
			only1 += 1
			only2 += 1
			ne2.remove(ne)
		else:
			only1 += 1
	only2 += len(ne2)
	if only1 == 0:
		return 0.0
	return float(mutual - only1) / only1

def ne_overlap2(sent1, sent2):
        ne1, word1 = get_tag(sent1, 4, '', False)
        ne2, word2 = get_tag(sent2, 4, '', False)

        mutual = 0
        only1 = 0
        only2 = 0
        for ne in ne1:
                if ne in ne2:
                        mutual += 1
                        only1 += 1
                        only2 += 1
                        ne2.remove(ne)
                else:
                        only1 += 1
        only2 += len(ne2)
        if only1 == 0:
                return 0.0
        return float(mutual - only1) / only1


def nnp_overlap(sent1, sent2):
	nnp1, word1 = get_tag(sent1, 2, 'NNP', True)
	nnp2, word2 = get_tag(sent2, 2, 'NNP', True)

        mutual = 0
        only1 = 0
        only2 = 0
        for word in word1:
                if word in word2:
                        mutual += 1
                        only1 += 1
                        only2 += 1
                        word2.remove(word)
                else:
                        only1 += 1
        only2 += len(word2)
        if only1 == 0:
                return 0.0
        return float(mutual - only1) / only1

def unk_overlap(sent1, sent2):
	unk1, word1 = get_tag(sent1, 3, '', False)
	unk2, word2 = get_tag(sent2, 3, '', False)

        mutual = 0
        only1 = 0
        only2 = 0
        for unk in unk1:
                if unk in unk2:
                        mutual += 1
                        only1 += 1
                        only2 += 1
                        unk2.remove(unk)
                else:
                        only1 += 1
        only2 += len(unk2)
        if only1 == 0:
                return 0.0
        return float(mutual - only1) / only1
	
