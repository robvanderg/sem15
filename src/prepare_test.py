import string
import os

def clean(sentence):
	newsentence = ''
	for word in sentence.split():
		wordArray = word.split('/')
		newsentence = '%s%s/%s\n' %(newsentence, wordArray[0], wordArray[2])
	return newsentence

index = 20000
for line in open('../data/test.data'):
	dir = os.path.join('../test', str(index))
	if(not os.path.isdir(dir)):
		os.makedirs(dir)
	index += 1

	splitted_line = line.split("\t")

	id_file = open(os.path.join(dir, 'id'), 'w')
	id_file.write(splitted_line[0])
	id_file.write('\n')
	id_file.close()

	topic_file = open(os.path.join(dir, 'topic'), 'w')
	topic_file.write(splitted_line[1])
	topic_file.write('\n')
	topic_file.close()

	tweet1_file = open(os.path.join(dir, 't'), 'w')
	tweet1_file.write(splitted_line[2])
	tweet1_file.write('\n')
	tweet1_file.close()

	tweet2_file = open(os.path.join(dir, 'h'), 'w')
	tweet2_file.write(splitted_line[3])
	tweet2_file.write('\n')
	tweet2_file.close()


	tweet1pos_file = open(os.path.join(dir, 't.pos'), 'w')
	tweet1pos_file.write(clean(splitted_line[4]))
	tweet1pos_file.close()

	tweet2pos_file = open(os.path.join(dir, 'h.pos'), 'w')
	tweet2pos_file.write(clean(splitted_line[5]))
	tweet2pos_file.close()

print ('finished!')
