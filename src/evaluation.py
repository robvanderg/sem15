from sklearn import linear_model

#0.85, -3.11, 1.7
def guess(source, lr, lr2,  treshold1, treshold2, treshold3): 
	probs = lr.predict_proba(source)[0]
	score1 = 0
	for idx in range(0,len(probs)):
		score1 += idx * probs[idx]
	score2 = lr2.predict_proba(source)[0][0]
	
	if score2 > 0.75:
		return False
	
	if score1 > treshold3:
		return True

	probs2 = lr.decision_function(source)[0]
	total = 0.0
	for idx in range(0, len(probs2)):
		total += idx * probs2[idx] * probs[idx]
	if total > treshold2:
		return True
	return False

def get_sim(source, lr):	
	probs = lr.predict_proba(source)[0]
	score = 0
	for idx in range(0,len(probs)):
		score += idx * probs[idx]
	return score/5

def write(sources, lr, lr2, treshold1, treshold2, treshold3):
	wf = open('../data/PIT2015_ROB_01_all.output', 'w')
	for source in sources:
		if guess(source, lr, lr2, treshold1, treshold2, treshold3) == True:
			wf.write('true\t')
		else:
			wf.write('false\t')
		wf.write("%.4f" % get_sim(source, lr))
		wf.write('\n')
	wf.close()

def evaluate(sources, targets, lr, lr2, treshold1, treshold2, treshold3):
	leftout = 0
	tp = 0
	fp = 0
	tn = 0
	fn = 0
	pos = 0
	neg = 0
	for idx in range(0,len(sources)):
		if targets[idx] == 2:
			leftout += 1
		elif targets[idx] < 2:
			neg += 1
			if guess(sources[idx], lr, lr2, treshold1, treshold2, treshold3) == False:
				tn += 1
			else:
				fp += 1
		elif targets[idx] > 2:
			pos += 1
			if guess(sources[idx], lr, lr2, treshold1, treshold2, treshold3) == True:
				tp += 1
			else:
				fn += 1
	
	print 'tp: ' + str(tp)
	print 'fp: ' + str(fp)
	print 'tn: ' + str(tn)
	print 'fn: ' + str(fn)
	print 'leftout: ' + str(leftout) + '\n'
	
	prec = float(tp) / (tp + fp)
	rec = float(tp) / (tp + fn)
	f = 2 * (prec * rec) / (prec + rec)
	acc = float(tp + tn) / (tp + fp + tn + fn)
	
	print 'acc:  ' + str(acc)
	print 'tres1: ' + str(treshold1)
	print 'tres2: ' + str(treshold2)
	print 'tres3: ' + str(treshold3)
	print 'prec: ' + str(prec)
	print 'rec:  ' + str(rec)
	print 'f:    ' + str(f) + '\n'
	
	#print 'pos: ' + str(pos)
	#print 'neg: ' + str(neg)
	return f
