#!/usr/bin/python

"""
Parameters for semeval task 1
"""
import os

# Parameters
DEBUG = True            # More informative print-outs
USE_BIGRAMS = True     # Use bigrams for the DSM (Slightly worse results when this is switched on)
USE_TRIGRAMS = True     # Use trigrams for the DSM
RECALC_FEATURES = True
#RECALC_FEATURES = False # Remember to switch this to True if features are changed
WRITE_TO_MESH = False    # Write to mesh (ann)
POST_PROCESS = False    # Post-process by making sure values are between 1.0 and 5.0
USE_BOXER = False        # Use boxer features
WRITE_COMPLEXITY = False # Write DRS complexity

# Paths
working_path = '../working/'               # Directory containing word embeddings
working2 = '../working2.bak/'
